#! preprocessing

import sys, subprocess, os

collapse_cmd = '/share_space/metagenome/biosoft/bbmap/bbmerge.sh loose=t mininsert=25 mininsert0=25 qtrim2=t trimq=6 in1={1} in2={2} out={0}.collapsed.fastq.gz outu1={0}.u1.fastq.gz outu2={0}.u2.fastq.gz'
trim_cmd = '/share_space/metagenome/biosoft/bbmap/bbduk2.sh rref=/share_space/metagenome/database/adapters.fas fref=/share_space/metagenome/database/phiX.fas k=29 qtrim=r trimq=6 minlength=25 -in={0} -out={1}.rd{2}.fastq.gz entropy=0.8 overwrite=true entropywindow=25'
kraken_cmd = '/share_space/metagenome/biosoft/kraken/kraken -db /share_space/metagenome/database/minikraken_20141208/ --thread 4 --fastq-input --gzip-compressed {0}.final.fastq.gz > {0}.kraken && /share_space/metagenome/biosoft/kraken/kraken -db /share_space/metagenome/database/minikraken_20141208/ {0}.kraken > {0}.kraken_report && rm {0}.kraken'


if __name__ == '__main__' :
    list_file = sys.argv[1]
    samples = {}
    with open(list_file) as fin :
        for line in fin :
            sample, fname = line.strip().split()
            if sample in samples :
                samples[sample].append(fname)
            else :
                samples[sample] = [fname]
    for sample, files in samples.iteritems() :
        if len(files) > 1 :
            subprocess.Popen(collapse_cmd.format(sample, *files).split()).communicate()
            files = [sample + '.collapsed.fastq.gz', sample + '.u1.fastq.gz', sample + '.u2.fastq.gz']
        rf = []
        for id, fname in enumerate(files) :
            if not fname.endswith('collapsed.fastq.gz') :
                subprocess.Popen(trim_cmd.format(fname, sample, id).split()).communicate()
                rf.append('{0}.rd{1}.fastq.gz'.format(sample, id))
            else :
                rf.append('{0}.collapsed.fastq.gz'.format(sample))
        subprocess.Popen('zcat {0} | gzip -c > {1}'.format(' '.join(rf), '{0}.final.fastq.gz'.format(sample)), shell=True).communicate()
        for f in rf :
            os.unlink(f)
        for fname in [sample + '.u1.fastq.gz', sample + '.u2.fastq.gz'] :
            if os.path.isfile(fname) :
                os.unlink(fname)
    for sample in samples :
        subprocess.Popen(kraken_cmd.format(sample), shell=True).communicate()