import subprocess, sys, gzip, json, time
import math, random, ete3, numpy as np

encode = {'A':1, 'C':2, 'G':4, 'T':8, 'a':1, 'c':2, 'g':4, 't':8}

def norm_twotails(u,sigma,x):
    T=(0.0705230784,0.0422820123,0.0092705272,0.0001520143,0.0002765672,0.0000430638)
    x2=(x-u)/sigma
    x3=abs(x2)/math.sqrt(2)
    E=1-pow((1+sum([a*pow(x3,(i+1)) for i,a in enumerate(T)])),-16)
    p=2*(0.5-0.5*E)
    return(p)

def load_bam(bam_file, sites, debug=0) :
    if debug :
        try :
            arr = [{int(k):v for k,v in ar.iteritems()} for ar in json.load(open('debug', 'rb'))]
            return arr
        except :
            pass
    arr = [{1:0, 2:0, 4:0, 8:0} for id in xrange(len(sites))]
    
    indel_num = 0
    bam_mpileup = subprocess.Popen('/enterotool/bin/samtools mpileup {0}'.format(bam_file).split(), stdout=subprocess.PIPE)
    prev = ''
    for line in iter(bam_mpileup.stdout.readline, r''):
        part = line.strip().split()
        if prev == '' :
            prev = part[0]
        elif prev != part[0] :
            break

        site = sites.get('\t'.join(part[:2]), -1)
        if site >= 0 and len(part) > 4 :
            bases = part[4]
            for id in xrange(len(bases)) :
                b = bases[id]
                if b in ('+', '-') :
                    indel_num = re.findall('^(\d+)', bases[(id+1):])[0]
                    indel_num = len(indel_num) + int(indel_num)
                elif indel_num >0 :
                    indel_num -= 1
                elif b in 'aAcCgGtT' :
                    arr[site][encode[b]] += 1
    if debug :
        json.dump(arr, open('debug', 'wb'))
    return arr

def assignSNVset (branches, states, snv) :
    total_snv = sum([len((set(s1) | set(s2))-set([0])) for s1, s2 in zip(states, snv)])
    sample_snv = sum([len(s) for s in snv])
    
    snv_barcode = np.array([sum(s) for s in snv]).reshape(-1, 1)
    presence = (states & snv_barcode) > 0

    max_s = 0.0
    min_u = 1.0
    brs = []
    
    for m,br in enumerate(branches) :
        br_supports_list = presence.T[br[0]] | presence.T[br[1]]
        unsupported = np.where(br_supports_list == 0)
        unsupported_snvs = np.sum((states.T[br[0]][unsupported] - states.T[br[1]][unsupported] != 0) & (states.T[br[0]][unsupported] != 0) & (states.T[br[1]][unsupported] != 0)) + \
            np.sum(states.T[br[0]][unsupported]+states.T[br[1]][unsupported] > 0)
        br_support = np.sum(br_supports_list)
        br_snv = unsupported_snvs + br_support

        if br_snv > 0 and 1.0*br_support/br_snv > max_s:
            max_s = 1.0*br_support/br_snv
        if 1.0*(sample_snv-br_support)/(total_snv-br_snv) < min_u:
            min_u = 1.0*(sample_snv-br_support)/(total_snv-br_snv)
        brs.append([br_support, br_snv-br_support, sample_snv-br_support, total_snv-br_snv-(sample_snv-br_support)])
    max_s = max_s if max_s < 0.9999 else 0.9999
    min_u = min_u if min_u > 0.0001 else 0.0001
    mat = [math.log(max_s), math.log(1-max_s), math.log(min_u), math.log(1-min_u)]
    br_lk = sorted([ [mat[0]*br_info[0]+mat[1]*br_info[1]+mat[2]*br_info[2]+mat[3]*br_info[3], '\t'.join([str(x) for x in br_info]), br[2] ] for br_info, br in zip(brs, branches) ], key=lambda lk:lk[0], reverse=True)
    return(br_lk)

def load_anc(state_filename) :
    sites = {}
    states = []
    with gzip.open(state_filename, 'r') as state_file:
        head = state_file.readline().strip().split('\t')
        nodes = {name:id for id, name in enumerate(head[2:])}
        for line in state_file:
            parts = line.strip().split('\t')
            site = '\t'.join(parts[:2])
            sites[site] = len(states)
            states.append([ encode[base] for base in parts[2:len(head)]])
    return sites, nodes, np.array(states)

def get_branches(phy, nodes) :
    branches = []
    for node in phy.iter_descendants() :
        if node.name in nodes and node.up.name in nodes :
            branches.append([nodes[node.up.name], nodes[node.name], node.name])
    if phy.name not in nodes and len(phy.children) == 2 and phy.children[0].name in nodes and phy.children[1].name in nodes : 
        if phy.children[0].dist <= phy.children[1].dist :
            branches.append([nodes[phy.children[0].name], nodes[phy.children[1].name], phy.children[1].name])
        else :
            branches.append([nodes[phy.children[1].name], nodes[phy.children[0].name], phy.children[0].name])
    return branches

if __name__ == '__main__' :
    phy_filename = sys.argv[1]
    state_filename = sys.argv[2]
    bam_filename = sys.argv[3]
    permutation_number = int(sys.argv[4]) if len(sys.argv) > 4 else 100
    max_iteration = int(sys.argv[5]) if len(sys.argv) > 5 else 5

    phy = ete3.Tree(newick=phy_filename, format=1)
    sites, nodes, states = load_anc(state_filename)
    branches = get_branches(phy, nodes)

    sample = load_bam(bam_filename, sites, debug=0)

    assigned_nodes = []
    for ids in range(max_iteration) :
        depths = sorted([ site for site in [ max(site.values()) for site in sample ] if site > 0])
        if len(depths) == 0 :
            break
        low_depth = max(depths[len(depths)/2]/3, 1)

        sample_snv = [ [ b for b, n in site.iteritems() if n >=low_depth ] for site in sample ]

        sortedLK = assignSNVset(branches, states, sample_snv)

        total_snvs = [[id, b] for id, base in enumerate([list((set(s1) | set(s2))-set([0])) for s1, s2 in zip(states, sample_snv)]) for b in base]
        sample_cnt = sum([len(s) for s in sample_snv])
        
        p = 1.0
        randLK = []
        #delta_t = [0.0, 0.0, 0.0]        
        for ite in xrange(permutation_number):
            #t1= time.time()
            rand_snv = [[] for id in xrange(len(sample_snv))]
            #delta_t[0] += time.time() - t1
            for id, value in random.sample(total_snvs, sample_cnt) :
                rand_snv[id].append(value)
            #delta_t[1] += time.time() - t1
            randLK.append(assignSNVset(branches, states, rand_snv)[0][0])
            #delta_t[2] += time.time() - t1
            #print delta_t[0], delta_t[1]-delta_t[0], delta_t[2]-delta_t[1]            
            if ite%5 == 0 and ite > 0:
                randLK_mean = sum(randLK)/len(randLK)
                randLK_sigma = math.sqrt(sum([math.pow(lk - randLK_mean, 2) for lk in randLK])/(len(randLK)-1))
                p = norm_twotails(randLK_mean, randLK_sigma, sortedLK[0][0])
                if p < 0.01/(permutation_number-ite) or p >= 1-(1-0.01)**(permutation_number-ite):
                    break
        LK_max = max([x[0] for x in sortedLK])
        sortedPr = []
        for lk,pattern,br in sortedLK:
            if lk-LK_max >= -50:
                sortedPr.append([math.exp(lk-LK_max), pattern, br])
            else :
                sortedPr.append([math.exp(LK_max-50), pattern, br])
        Pr_sum = sum([pr[0] for pr in sortedPr])
        for pr in sortedPr :
            pr[0] = pr[0]/Pr_sum

        br = [br for br in branches if br[2] == sortedLK[0][2]][0]
        dists = [0, 0]
        for id, (site, b1, b2) in enumerate(zip(sample, states.T[br[0]], states.T[br[1]])) :
            if b1 != b2 :
                if site.get(b1,0) >= low_depth: dists[0] += site[b1]
                if site.get(b2,0) >= low_depth: dists[1] += site[b2]
            if site.get(b1,0) == site.get(b2,0) :
                site[b1] = site[b2] = 0
                states[id][np.where(states[id] == b1)] = 0
                states[id][np.where(states[id] == b2)] = 0
            elif site.get(b1,0) > site.get(b2,0) :
                site[b1] = 0
                states[id][np.where(states[id] == b1)] = 0
            else :
                site[b2] = 0
                states[id][np.where(states[id] == b2)] = 0

        print 'Iteration {0}. Best branch: {1} (Position {3}). Pr from permutation: {2}. Detail: '.format(ids, sortedPr[0][2], p, float(dists[1])/sum(dists) if sum(dists) > 0 else 'puncertain')
        print 'lk_prop\tSNV_match\tSNV_missing\tSNV_uncover\tSNV_remaining\tBranch\tBrLen_prop'
        for pr in sortedPr:
            if pr[0] >= 0.01:
                print '\t'.join([str(x) for x in pr])
        if p >= 0.01 :
            break
