# sigma integrated
import sys, subprocess, os, gzip, re, shutil
from multiprocessing import Pool

params = dict(
    sigma = '/home/zhemin/biosoft/Sigma/bin/sigma', 
    bowtie2_build = '/home/zhemin/bin/bowtie2-build -f', 
    bowtie2 = '/home/zhemin/bin/bowtie2', 
    samtools = '/usr/bin/samtools'
)


def sigma_index(source, taxon_list, dbname, n_thread=1, **params) :
    # read taxon_list
    taxa = {}
    with open(taxon_list) as fin, gzip.open('{0}.taxon.gz'.format(dbname), 'w') as fout :
        for line in fin :
            part = line.strip().split()
            if part[1] in taxa :
                raise ValueError('The same sequence name {0} present multiple times in taxon_list.'.format(part[1]))
            taxon = ''.join([ c for c in part[0] if c.isalnum() or c in '_.'])
            taxa[part[1]] = taxon
            fout.write('{0}\t{1}\n'.format(taxon, part[1]))
    
    # split reference sequence
    if source[-3:] == '.gz' :
        fin = gzip.open(source)
    else :
        fin = open(source)

    if n_thread > 1 :
        pool = Pool(processes=n_thread)

    sequences = {}
    to_run = []
    prev, seq_len, db_id = '', 0, 0
    fout = open('{0}.{1}'.format(dbname, db_id), 'w')
    for line in fin :
        if line[0] == '>' :
            name = line[1:].strip().split()[0]
            taxon = taxa.get(name, '')
            if taxon != prev :
                if prev != '' :
                    if seq_len > 3900000000 :
                        seq_len = sum([ len(''.join(s)) for n, s in sequences.iteritems() ])
                        fout.close()
                        if n_thread > 1 :
                            to_run.append('{0}.{1}'.format(dbname, db_id))
                        else :
                            run_build('{0}.{1}'.format(dbname, db_id))
                        db_id += 1
                        fout = open('{0}.{1}'.format(dbname, db_id), 'w')
                    for n, s in sequences.iteritems() :
                        fout.write('>{0}\n{1}\n'.format(n, ''.join(s)))
                prev = taxon
                sequences = {}
            if prev != '' :
                sequences[name] = []
        elif prev != '' :
            s = line.strip().split()
            sequences[name].extend(s)
            seq_len += sum([len(x) for x in s])
    fin.close()
    for n, s in sequences.iteritems() :
        fout.write('>{0}\n{1}\n'.format(n, ''.join(s)))
    fout.close()    
    if n_thread > 1 :
        to_run.append('{0}.{1}'.format(dbname, db_id))
        pool.map(run_build, to_run)
        pool.close()
    else :
        run_build('{0}.{1}'.format(dbname, db_id))

    # run bowtie2-build on each split
    return dbname

def run_build(filename) :
    run = subprocess.Popen('{0} {1} {1}'.format(params['bowtie2_build'], filename).split())
    run.communicate()
    if not run.returncode :
        subprocess.Popen(['gzip', filename]).communicate()
    return run.returncode

def sigma_align(dbname, workspace, r1, r2=None, n_thread=10, n_mismatch=3, ins_min=0, ins_max=1000, **params) :
    # prepare databases
    taxa = {}
    with gzip.open('{0}.taxon.gz'.format(dbname)) as fin :
        for line in fin :
            part = line.strip().split()
            taxa[part[1]] = part[0]
    taxa_list = list(set(taxa.values()))

    db_list = []
    for id in xrange(100) :
        if os.path.exists('{0}.{1}.rev.1.bt2'.format(dbname, id)) or os.path.exists('{0}.{1}.rev.1.bt2l'.format(dbname, id)) :
            db_list.append('{0}.{1}'.format(dbname, id))

    # prepare workspace
    if os.path.exists(os.path.join(workspace, 'sigma_alignments_output')) :
        shutil.rmtree(os.path.join(workspace, 'sigma_alignments_output'))
    os.makedirs(os.path.join(workspace, 'sigma_alignments_output'))
    for taxon in list(set(taxa.values())) :
        if os.path.exists(os.path.join(workspace, 'sigma_alignments_output', taxon)) :
            shutil.rmtree(os.path.join(workspace, 'sigma_alignments_output', taxon))
        os.makedirs(os.path.join(workspace, 'sigma_alignments_output', taxon))

    with open(os.path.join(workspace, 'r1.fastq'), 'w') as fout :
        if r1[-3:] == '.gz' :
            fin = gzip.open(r1)
        else :
            fin = open(r1)
        for line in fin :
            fout.write(line)
        fin.close()
    if r2 is None :
        read_info = '-U {0}'.format(os.path.join(workspace, 'r1.fastq'))
        with open(os.path.join(workspace, 'sigma.cfg'), 'w') as fout :
            fout.write('''[Program_Info]
Bowtie_Directory={0}
Samtools_Directory={1}
[Model_Probability]
Mismatch_Probability=0.05
Minimum_Relative_Abundance = 0.000001
[Statistics]
Bootstrap_Iteration_Number=200
[Data_Info]
Reference_Genome_Directory=sigma_alignments_output
Single_End_Reads=r1.fastq
'''.format(os.path.dirname(os.path.realpath(params['bowtie2'])), os.path.dirname(os.path.realpath(params['samtools']))))
    else :
        with open(os.path.join(workspace, 'r2.fastq'), 'w') as fout :
            if r2[-3:] == '.gz' :
                fin = gzip.open(r2)
            else :
                fin = open(r2)
            for line in fin :
                fout.write(line)
            fin.close()
        read_info = '-1 {0} -2 {1}'.format(os.path.join(workspace, 'r1.fastq'), os.path.join(workspace, 'r2.fastq'))
        with open(os.path.join(workspace, 'sigma.cfg'), 'w') as fout :
            fout.write('''[Program_Info]
Bowtie_Directory={0}
Samtools_Directory={1}
[Model_Probability]
Mismatch_Probability=0.05
Minimum_Relative_Abundance = 0.000001
[Statistics]
Bootstrap_Iteration_Number=200
[Data_Info]
Reference_Genome_Directory=sigma_alignments_output
Paired_End_Reads_1=r1.fastq
Paired_End_Reads_2=r2.fastq
'''.format(os.path.dirname(os.path.realpath(params['bowtie2'])), os.path.dirname(os.path.realpath(params['samtools']))))

    # run bowtie2 in multiple threads
    map_summary = { taxon:0 for taxon in taxa_list }
    for db_prefix in db_list :
        heads = {}
        results = { taxon:{} for taxon in taxa_list }
        cmd_bt2 = '{0} -k 300 -t --no-unal --no-discordant --no-mixed --ignore-quals --score-min L,15,1.2 --mp 8,8 --sensitive-local --np 8 -q -p {2} -I {3} -X {4} -x {5} {6}'.format(
            params['bowtie2'], n_mismatch*-6, n_thread, ins_min, ins_max, db_prefix, read_info)
        run_bt2 = subprocess.Popen(cmd_bt2.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        for line in iter(run_bt2.stdout.readline, r'') :
            if line[0] == '@' : 
                if line[:3] == '@SQ' :
                    part = line.strip().split()
                    name = part[1][3:]
                    taxon = taxa[name]
                    if taxon not in heads :
                        heads[taxon] = []
                    heads[taxon].append(line)
                continue
            part = line.strip().split()
            if len(part[5].split('S')) > 2 :
                continue
            m = 0
            for p in reversed(part) :
                if p.startswith('NM:i:') and int(p[5:]) > n_mismatch :
                    m = 1
                    break
            if m : 
                continue
            if part[2] in taxa :
                taxon = taxa[part[2]]
                if part[0] not in taxon :
                    results[ taxon ][part[0]] = line
                    map_summary[ taxon ] += 1
        # split results into folder structures
        for taxon, reads in results.iteritems() :
            if taxon in heads :
                with gzip.open(os.path.join(workspace, 'sigma_alignments_output', taxon, '{0}.header.gz'.format(taxon)), 'a') as fout :
                    fout.write(''.join(heads[taxon]))
            if len(reads) > 0 :
                with gzip.open(os.path.join(workspace, 'sigma_alignments_output', taxon, '{0}.sam.gz'.format(taxon)), 'a') as fout :
                    for n, r in reads.iteritems() :
                        fout.write(r)
    log = run_bt2.stderr.read()
    n_read = int(re.findall(r'(\d+) reads; of these', log)[0])
    
    # create virtual log files
    #map(formalize_bam, [dict(sam_prefix=os.path.join(workspace, 'sigma_alignments_output', taxon, taxon), n_read=n_read, n_align=map_summary[taxon]) for taxon in taxa_list])
    if n_thread <= 1 :
        map(formalize_bam, [dict(sam_prefix=os.path.join(workspace, 'sigma_alignments_output', taxon, taxon), head=os.path.join(workspace, 'sam.head.gz'.format(taxon)), n_read=n_read, n_align=map_summary[taxon]) for taxon in taxa_list])
    else :
        pool = Pool(processes=n_thread)
        pool.map(formalize_bam, [dict(sam_prefix=os.path.join(workspace, 'sigma_alignments_output', taxon, taxon), head=os.path.join(workspace, 'sam.head.gz'.format(taxon)), n_read=n_read, n_align=map_summary[taxon]) for taxon in taxa_list])
    return workspace

def formalize_bam(param) :
    if os.path.exists('{0}.sam.gz'.format(param['sam_prefix'])) :
        cmd_samtools = 'gzip -cd {1}.header.gz {1}.sam.gz | {0} view -bo {1}.align.bam -'.format(params['samtools'], param['sam_prefix'])
    elif os.path.exists('{0}.header.gz'.format(param['sam_prefix'])) :
        cmd_samtools = 'gzip -cd {1}.header.gz | {0} view -bo {1}.align.bam -'.format(params['samtools'], param['sam_prefix'])
    else :
        cmd_samtools = 'echo "@SQ\tSN:None\tLN:1\n" | {0} view -bo {1}.align.bam -'.format(params['samtools'], param['sam_prefix'])
    run = subprocess.Popen(cmd_samtools, shell=True)
    run.communicate()
    if not run.returncode :
        if os.path.exists('{0}.sam.gz'.format(param['sam_prefix'])) : 
            os.unlink('{0}.sam.gz'.format(param['sam_prefix']))
        if os.path.exists('{0}.header.gz'.format(param['sam_prefix'])) :  
            os.unlink('{0}.header.gz'.format(param['sam_prefix']))
    with open('{0}.align.log'.format(param['sam_prefix']), 'w') as fout :
        fout.write('''Time loading reference: 00:00:00
Time loading forward index: 00:00:00
Time loading mirror index: 00:00:00
Multiseed full-index search: 00:00:00
{0} reads; of these:
  {0} (100%) were unpaired; of these:
    {1} ({4:.2f}%) aligned 0 times
    {2} ({5:.2f}%) aligned exactly 1 time
    0 (0.00%) aligned >1 times
{3:.8f}% overall alignment rate
Time searching: 00:00:00
Overall time: 00:00:00'''.format(param['n_read'], param['n_read'] - param['n_align'], param['n_align'], float(param['n_align'])*100.0/param['n_read'], 
                         100 - float(param['n_align'])*100.0/param['n_read'], float(param['n_align'])*100.0/param['n_read']))
    return run.returncode

def sigma(workspace, n_thread=10, **params) :
    # prepare sigma_config
    os.chdir(workspace)
    # run sigma
    subprocess.Popen('{0} -t {1} -c sigma.cfg -w ./'.format(params['sigma'], n_thread).split()).communicate()
    # modify sigma results
    if os.path.exists('r1.fastq') :
        os.unlink('r1.fastq')
    if os.path.exists('r2.fastq') :
        os.unlink('r2.fastq')
    return workspace + '.sigma.output'


if __name__ == '__main__' :
    try :
        cmd = sys.argv[1]
    except :
        print 'query: dbname, workspace, r1, r2, n_thread, n_mismatch, ins_min, ins_max'
        print 'index: dbname, taxon_list, source'
        sys.exit(1)
    params.update(dict([x.split('=',1) for x in sys.argv[2:]]))

    for numeric_param in ['n_thread', 'n_mismatch', 'ins_min', 'ins_max'] :
        if numeric_param in params :
            params[numeric_param] = int(params[numeric_param])


    if cmd == 'index' :
        for essential_param in ['dbname', 'taxon_list', 'source'] :
            assert params[essential_param]
        sigma_index(**params)
    elif cmd == 'query' :
        for essential_param in ['r1', 'workspace', 'dbname'] :
            assert params[essential_param]
        if sigma_align(**params) :
            sigma(**params)
    else :
        print 'query: dbname, workspace, r1, r2, n_thread, n_mismatch, ins_min, ins_max'
        print 'index: dbname, taxon_list, source'
        sys.exit(1)
