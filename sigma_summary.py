import sys, os
from subprocess import Popen, PIPE
from multiprocessing import Pool


def prop_check(data) :
    ref, v, files = data
    if sum(v) < 20 :
        return (ref, 0.0)
    try :
        l_sum = 0
        seq = {}
        for i, (fname, n) in enumerate(zip(files, v)) :
            if i == 0 :
                bam = Popen('/home/zhemin/bin/samtools view -h {0}'.format(os.path.join(fname, 'sigma_alignments_output', ref, ref + '.align.bam')).split(), stdout=PIPE)
            else :
                bam = Popen('/home/zhemin/bin/samtools view {0}'.format(os.path.join(fname, 'sigma_alignments_output', ref, ref + '.align.bam')).split(), stdout=PIPE)
            s = {}
            for line in iter(bam.stdout.readline, r'') :
                if line.startswith('@SQ') :
                    part = line.strip().split()
                    l = int(part[2][3:])
                    l_sum += l
                    for id in xrange(0, l, 300) :
                        seq[(part[1][3:], id)] = 0.0
                else :
                    part = line.strip().split()
                    s[(part[2], (int(part[3])/300)*300)] = s.get((part[2], (int(part[3])/300)*300), 0.0) + 1.0
            p = n/sum(s.values())
            for k, kk in s.iteritems() :
                seq[k] += kk*p
        median = sorted(seq.values())[len(seq)/2]
        cap = max(median*3.0, 1.0)
        n = sum([min(s, cap) for s in seq.values()])
        sys.stderr.write('Recheck\t{0}\t{1}\t{2}\n'.format(ref, sum(v), n))
        return (ref, n)
        #frac = sum([ s[0]*min(s[1], 1.0) for s in seq.values() if s[1] > 0])/ float(l_sum)
        #if frac <= 0.3 :
            #n = sum([ min(s[1], 1.0) for s in seq.values() if s[1] > 0])
            #if n < v :
                #sys.stderr.write('Recheck\t{0}\t{1}\t{2}\t{3}\n'.format(ref, v, frac, n))
                #return (ref, float(n))
    except :
        return (ref, 0.0)
    return (ref, sum(v))


if __name__ == '__main__' :
    fnames = {}
    with open(sys.argv[1]) as flist :
        for line in flist :
            part = line.strip().split()
            if part[0] not in fnames :
                fnames[part[0]] = []
            fnames[part[0]].append(part[1])
    
    proportions = {}
    sample_reads = {}
    pool = Pool(8)
    for sample, files in fnames.iteritems() :
        total_read = 0
        prop = {}
        for id, fname in enumerate(files) :
            with open(os.path.join(fname, 'sigma_out.gvector.txt')) as fin :
                names = {}
                for line in fin :
                    if line.startswith('+') :
                        part = line.strip().split('\t')
                        read_num = int(part[2])
                        total_read += read_num
                    elif line.startswith('@') :
                        part = line.split('\t')
                        names[part[1]] = part[2]
                    elif line.startswith('*') :
                        part = line.strip().split('\t')
                        v = float(part[3])*read_num/100.0
                        if names[part[1]] not in prop :
                            prop[ names[part[1]] ] = [0.0 for i in xrange(id)]
                        prop[ names[part[1]] ].append(v)
        sys.stderr.write('{0}\t{1}\n'.format(sample, total_read))
        #for ref, v in sorted(prop.items(), key=lambda x:x[1]) :
            #prop_check([ref,v,files])
        prop = dict(pool.map(prop_check, [[ref, v, files] for ref, v in sorted(prop.items(), key=lambda x:sum(x[1]))]))
            
        proportions[ sample ] = prop
        sample_reads[ sample ] = total_read/100.0
    
    rows = {}
    for fname, prop in proportions.iteritems() :
        for s, p in prop.iteritems() :
            if s not in rows or rows[s] < p :
                rows[s] = p
    
    rows = sorted([ r for r in rows if rows[r] > 50.0 ], key=lambda x:rows[x], reverse=True)
    cols = sorted(proportions.keys())
    print '\t'+'\t'.join(cols)
    for row in rows :
        sys.stdout.write(row)
        for sample in cols :
            sys.stdout.write('\t{0}'.format( proportions[sample].get(row, 0.0)/sample_reads[ sample ] ))
        print ''